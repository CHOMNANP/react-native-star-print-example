/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  DeviceEventEmitter
} from "react-native";
import { StarPRNT, Emulation } from "react-native-star-prnt";

const EmulationPrinter = 'StarGraphic';

export default class App extends Component {

  

  componentDidMount() {
    this.listener = Platform.select({
      ios: () => {
        return StarPRNT.StarPRNTManagerEmitter.addListener("starPrntData", e =>
          console.log(e)
        );
      },
      android: () => {
        return DeviceEventEmitter.addListener("starPrntData", e => { console.log('starPrntData event',e); }
        );
      }
    })();
  }


  connect = async(_emulation,_portName)=> {
    let isConnect = false;
    try {
      isConnect = await StarPRNT.connect(_portName, _emulation, false);
      console.log('connect success',isConnect); // Printer Connected!
    } catch (e) {
      console.log('connect error',e);
    }
    return isConnect;
  }

  print = async(_emulation,_portName,_commands)=> {
    try {
      var printResult = await StarPRNT.print(_emulation, _commands, _portName);
      console.log(printResult); // Success!
    } catch (e) {
      console.log(e);
    }
  }


  getCommands(){

    let commands = [];    
    commands.push({append:
      "Star Clothing Boutique\n" +
      "123 Star Road\n" +
      "City, State 12345\n" +
      "\n"});
    commands.push({appendLineSpace:500});
    commands.push({appendBytes:[0x9c],appendCodePage:"CP858",appendInternational:"UK",appendEncoding:"USASCII"}); 
    commands.push({appendEmphasis:"SALE\n"});
    commands.push({appendQrCode:"{BStar", QrCodeModel:"No2", QrCodeLevel:"H", cell: 20, alignment:"Center" });    
    commands.push({appendBarcode:"{BThis is Beautifule", BarcodeSymbology:"Code128", BarcodeWidth:"Mode2", height:100, hri:true });
    commands.push({append:
      "Star Clothing Boutique\n" +
      "123 Star Road\n" +
      "City, State 12345\n" +
      "\n"});
    commands.push({appendLineSpace:500});
    commands.push({appendCutPaper: StarPRNT.CutPaperAction.PartialCutWithFeed});

    return commands;
  }


  handlePrint = async () => {

    try {
      console.log("started===================>");
      this.printers = await StarPRNT.portDiscovery("All");      

      if(this.printers.length){
        let prnt = this.printers[0];
        console.log("select printerprnt",prnt);
        let isConnected = await this.connect(EmulationPrinter,prnt.portName);    
        
        if(isConnected){
          console.log("printerprnt is connected, start printing",isConnected);

          let status = await StarPRNT.checkStatus(prnt.portName,EmulationPrinter);
          console.log("status",status);
          if(!status.offline){
            console.log("printer online, starting priiting");
            await this.print(
              EmulationPrinter,
              prnt.portName,
              this.getCommands());
          }
          
        }else{
          console.log("printerprnt is not connected",isConnected);
        }
        
      }else{
        console.log("no printer selected prnt");
      }
      console.log("stop========================>");
      
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this.handlePrint}
          style={{ width: 100, height: 100, backgroundColor: "gray" }}
        >
          <Text>Click here to Print</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
